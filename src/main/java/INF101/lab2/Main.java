package INF101.lab2;

import INF101.lab2.pokemon.IPokemon;
import INF101.lab2.pokemon.Pokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        pokemon1 = new Pokemon("Blomst");
        pokemon2 = new Pokemon("Gjøcob");

        System.out.println(pokemon1);
        System.out.println(pokemon2 + "\n");

        while (pokemon1.isAlive() && pokemon2.isAlive()) {
            System.out.println("\n" + pokemon1.getName() + " attacks " + pokemon2.getName() + ". \n");
            pokemon1.attack(pokemon2);
            if (pokemon2.isAlive() == false){
                break;
            }
            System.out.println(pokemon2.getName() + " attacks " + pokemon1.getName() + ". \n");
            pokemon2.attack(pokemon1);
        }
    if (pokemon1.isAlive()) {
        System.out.println( pokemon2.getName() + " is defeated by " + pokemon1.getName());
    }    
    else {
        System.out.println( pokemon1.getName() + " is defeated by " + pokemon2.getName());
    }
        

        // Have two pokemon fight until one is defeated
    }
}
