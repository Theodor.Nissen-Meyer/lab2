package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealtPoints;
    int strength;
    Random random;

    public Pokemon(String name) {
        this.name = name;
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealtPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;
    }

    @Override
    public int getCurrentHP() {
        return healthPoints;
    }

    @Override
    public int getMaxHP() {
        return maxHealtPoints;
    }

    public boolean isAlive() {
        return healthPoints > 0;
    }

    @Override
    public void attack(IPokemon target) {
        if (target.isAlive()){
            System.out.println(target.getName() + " is being attacked!");
            target.damage(strength);
        }
        else {
            System.out.println(target.getName() + " has 0 HP and is defeated.");
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken < 0){
            return;
        }
        healthPoints = getCurrentHP() - damageTaken;
        if (healthPoints < 0) {
            healthPoints = 0;
            
        }
        
        else {
            System.out.println(name + " takes " + damageTaken + " and is left with " + healthPoints +"/"+ maxHealtPoints + " HP");
        }
    }

    @Override
    public String toString() {
        return name + " HP: (" + healthPoints + "/" + maxHealtPoints + ") STR: " + strength;
    }

}
